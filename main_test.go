package main

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	// "strconv"
	// "encoding/json"
	// "bytes"
)

func executeResponse(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	return rr
}

func TestServer(t *testing.T) {
	req, _ := http.NewRequest("GET", "/", nil)
	w := httptest.NewRecorder()
	test(w, req)
	res := w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("Error expected to be nil, got %v", err)
	}
	if string(data) != "Hello" {
		t.Errorf("Expected Hello , got %v", string(data))
	} else {
		t.Logf("Passed! Expected Hello, got %v", string(data))
	}

}
